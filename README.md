# merise

MERISE est une méthode d'analyse et de conception des systèmes d'information (SI). Développée en France dans les années 1970, MERISE permet de structurer et de formaliser les étapes nécessaires pour concevoir un système d'information, en passant de l'expression des besoins à la réalisation technique.

La structure de la méthode MERISE se compose ainsi : 
Niveau Conceptuel:
    Modèle Conceptuel de Données (MCD) : Représente les entités, les associations entre ces entités et les contraintes d'intégrité, indépendamment de toute considération technique ou organisationnelle.
    Modèle Conceptuel de Traitements (MCT) : Décrit les processus métier et les règles de gestion, en se concentrant sur ce que le système doit faire sans préciser comment il le fait.
    
Niveau Organisationnel:
    Modèle Organisationnel de Données (MOD) : Adapte le MCD aux contraintes organisationnelles et aux structures de l'entreprise.
    Modèle Organisationnel de Traitements (MOT) : Précise comment les processus décrits dans le MCT seront réalisés au sein de l'organisation, en tenant compte des contraintes organisationnelles.

Niveau Physique:
    Modèle Physique de Données (MPD) : Traduction du MOD en termes techniques, spécifiant comment les données seront stockées dans un système de gestion de bases de données (SGBD) concret.
    Modèle Physique de Traitements (MPT) : Décrit comment les traitements seront implémentés techniquement, y compris les choix de technologies, les algorithmes, et les interfaces utilisateur.